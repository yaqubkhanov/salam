package dao;

import containers.Player;
import containers.enums.ResultCode;
import containers.results.BaseResult;
import dao.util.*;

import java.sql.SQLException;
import java.util.Map;

public class PlayerDaoImpl extends DB implements PlayerDao {
    @Override
    public Player getPlayerInfo(String uuid) throws SQLException {
        connect();
        Player player = new Player();
        try {
            preparedStatement = connection.prepareStatement("SELECT p.UUID,p.name,p.money,c.name as 'country' FROM PLAYERS p Join countries c on c.id=p.country_id where p.UUID = ?");
            preparedStatement.setString(1,uuid);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                player.setUUID(resultSet.getString("UUID"));
                player.setName(resultSet.getString("name"));
                player.setMoney(resultSet.getLong("money"));
                player.setCountry(resultSet.getString("country"));
            }
            preparedStatement.close();
        } finally {
            close();
        }
        return player;
    }

    @Override
    public void syncPlayerInfo(Player player) throws SQLException {
        connect();
        try {
            preparedStatement = connection.prepareStatement("UPDATE players p SET p.money = ? where p.UUID = ?");
            preparedStatement.setLong(1, player.getMoney());
            preparedStatement.setString(2,player.getUUID());
            preparedStatement.execute();
        } finally {
            close();
        }
    }

    @Override
    public void addPlayerActivity(String UUID, long activity) throws SQLException {
        connect();
        try {
            preparedStatement = connection.prepareStatement("insert into activity(activity,player_id) values (?,(Select p.id from players p where p.UUID = ?))");
            preparedStatement.setLong(1,activity);
            preparedStatement.setString(2,UUID);
            preparedStatement.execute();
        }finally {
            close();
        }
    }


}
