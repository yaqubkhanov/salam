package dao;

import containers.Player;
import containers.results.BaseResult;

import java.sql.SQLException;
import java.util.Map;

public interface PlayerDao {
    Player getPlayerInfo(String UUID) throws SQLException;

    void syncPlayerInfo(Player player) throws SQLException;

    void addPlayerActivity(String UUID,long activity) throws SQLException;
}
