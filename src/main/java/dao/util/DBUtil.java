package dao.util;




import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBUtil {
    static final String JDBC_DRIVER="com.mysql.jdbc.Driver";
    static final String DB_URL="jdbc:mysql://localhost:3306/carx";
    static final String USER="root";
    static final String PASS="root";
    public Connection connection =null;


    public Connection getConnection() {

        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);


        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection; }



}



