package dao.util;

import java.sql.*;

public class DB {
    protected PreparedStatement preparedStatement;
    protected ResultSet resultSet;
    protected Connection connection;

    protected void connect(){
        connection = new DBUtil().getConnection();
    }

    protected void close(){
        if(resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
