package utils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import com.google.gson.Gson;
import spark.ResponseTransformer;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Util {
    public static String toJson(Object object) {
        return new Gson().toJson(object);
    }
    public static ResponseTransformer json() {
        return Util::toJson;
    }
    public static Map<String, String> getFormParams(String request){
        List<NameValuePair> pairs = URLEncodedUtils.parse(request, Charset.defaultCharset());

        Map<String, String> map = new HashMap<>();
        for(int i=0; i<pairs.size(); i++){
            map.put(pairs.get(i).getName(), pairs.get(i).getValue());
        }
        return map;
    }
}