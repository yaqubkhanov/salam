package containers.enums;

public enum ResultCode {
    OK,
    NOT_FOUND,
    SYSTEM_ERROR,
    ILLEGAL_PARAMETERS
}
