package containers.results;

import containers.Player;
import containers.enums.ResultCode;

public class PlayerInfoResult extends BaseResult {
    private Player player;

    public PlayerInfoResult(ResultCode resultCode) {
        super(resultCode);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
