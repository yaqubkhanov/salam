package services;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.sun.org.apache.regexp.internal.RE;
import containers.Player;
import containers.enums.ResultCode;
import containers.results.BaseResult;
import containers.results.PlayerInfoResult;
import dao.PlayerDao;
import dao.PlayerDaoImpl;

import java.sql.SQLException;
import java.sql.Struct;
import java.util.Map;

public class PlayerService {
    PlayerDao playerDao;

    public PlayerService(PlayerDao playerDao) {
        this.playerDao = playerDao;
    }

    public PlayerInfoResult getPlayerInfo(String UUID){
        PlayerInfoResult playerInfoResult = new PlayerInfoResult(ResultCode.SYSTEM_ERROR);
        try {
            playerInfoResult.setPlayer(playerDao.getPlayerInfo(UUID));
            playerInfoResult.setResultCode(ResultCode.OK);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return playerInfoResult;
    }

    public BaseResult syncPlayerInfo(String request,String UUID){
        BaseResult result = new BaseResult(ResultCode.SYSTEM_ERROR);
        try {
            Gson gson = new Gson();
            Player player = gson.fromJson(request,Player.class);
            player.setUUID(UUID);
            playerDao.syncPlayerInfo(player);
            result.setResultCode(ResultCode.OK);

        }catch (SQLException e) {
            e.printStackTrace();
            result.setResultCode(ResultCode.SYSTEM_ERROR);
        }catch (JsonSyntaxException e){
            e.printStackTrace();
            result.setResultCode(ResultCode.ILLEGAL_PARAMETERS);
        }catch (Exception e){
            e.printStackTrace();
            result.setResultCode(ResultCode.SYSTEM_ERROR);
        }

        return result;
    }

    public BaseResult addPlayerActivity(String activity,String UUID){
        BaseResult result = new BaseResult(ResultCode.SYSTEM_ERROR);
        try {
            playerDao.addPlayerActivity(UUID,Long.parseLong(activity));
            result.setResultCode(ResultCode.OK);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
